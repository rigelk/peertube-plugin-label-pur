import store from "store2"

function register({ registerHook, peertubeHelpers }) {
  registerHook({
    target: "action:application.init",
    handler: async () => {
      if (store("peertube-plugin-label-pur:hide-label")) return

      // existing elements
      const baseStaticUrl = peertubeHelpers.getBaseStaticRoute()
      const imageUrl = baseStaticUrl + "/images/peertube_PUR.png"
      const body = document.getElementsByTagName("body")[0]

      // creating element
      var img_node = document.createElement("img")
      img_node.src = imageUrl

      var container_node = document.createElement("div")
      container_node.setAttribute("id", "label-pur")
      container_node.setAttribute("title", "Click to dismiss")

      var flip_card_inner = document.createElement("div")
      flip_card_inner.setAttribute("class", "flip-card-inner")
      var flip_card_front = document.createElement("div")
      flip_card_front.setAttribute("class", "flip-card-front")
      flip_card_front.appendChild(img_node)
      var flip_card_back = document.createElement("div")
      flip_card_back.setAttribute("class", "flip-card-back")
      flip_card_back.setAttribute("id", "label-pur-back")

      flip_card_inner.appendChild(flip_card_front)
      flip_card_inner.appendChild(flip_card_back)
      container_node.appendChild(flip_card_inner)

      // fetch stats
      const stats = await fetch("/api/v1/server/stats").then(res => res.json())
      if (stats.videosRedundancy.length === 0) return

      const prettyBytes = require("pretty-bytes")
      flip_card_back.innerHTML = `<div><strong>${prettyBytes(
        stats.videosRedundancy
          .map(e => e.totalUsed)
          .reduce((acc, val) => acc + val)
      )}</strong> </div><div style="font-size:x-small;line-height:0.8;">redunded</div>`

      // attach click event
      container_node.addEventListener("click", () => {
        container_node.hidden = true
        store("peertube-plugin-label-pur:hide-label", true)
      }, false)

      // final append to body
      body.appendChild(container_node)
    }
  })
}

export { register }
