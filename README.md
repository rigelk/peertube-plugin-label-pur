# Label PUR (Promoting and Using Redundancy)

A plugin that proudly shows you are using redundancy on your instance, and showing how much on hover.

![](http://lutim.cpy.re/B9jencM3.png)
